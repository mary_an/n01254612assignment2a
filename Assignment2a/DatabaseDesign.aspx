﻿<%@ Page Title="Database Design" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DatabaseDesign.aspx.cs" Inherits="Assignment2a.DatabaseDesign" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2>Aggregate Functions in SQL</h2>
    <h3>Overview</h3>
    <p>An <strong>aggregate function</strong> allows you to perform a calculation on a set of values to rturn a 
    single scalar value.All aggregate functions ignore NULL values except for the <code>COUNT</code> aggregate function.
    We often use aggtegate functions with the <code>GROUP BY</code> and <code>HAVING</code> clauses of the <code>SELECT</code> statement.<br />
    The following are the most commonly used SQL aggregate functions:<br />
        <code>COUNT</code> function returns the number of rows in a table satisfying the 
        criteria specified in the <code>WHERE</code> clause. It sets the number of rows or non NULL column values.<br />
        <code>AVG</code> function calculates the average value of a column of numeric type. It returns the average of all non NULL values.<br />
        <code>MIN</code> function is used to find the minimum value or lowest value of a column or expression. This function is 
        useful to determine the smallest of all selected values of a column.<br />
        <code>MAX</code> function is used to find the maximum value or highest value of a certain column or expression. 
        This function is useful to determine the largest of all selected values of a column.<br />
        <code>SUM</code> function returns the sum of all selected column.<br />
    </p>
    
    <pre>The general syntax for an aggregate function:
            
            SELECT aggregate_function(column_name) AS name, another_column_name
            FROM table_name
            WHERE condition
            GROUP BY another_column_name
            HAVING condition for groups
            ORDER BY
      
           *WHERE filters rows before they are grouped, and HAVING filters groups after grouping.
    </pre> 
</asp:Content>
<asp:Content ID="WebProgrammingExOne" ContentPlaceHolderID="MyCodeBox" runat="server">
    <h3>My examples:</h3>
    <asp:CodeBox ID="My_Database_Code" runat="server" code="DatabaseCode" owner="me"></asp:CodeBox>
    <p> This SELECT statement returns two columns for each vendor: vendor_name, payment_total sum as total. The result will be sorted
    in descending order according to the payment_total sum for each vendor.  
    </p> 
</asp:Content>
<asp:Content ID="DatabaseExTwo" ContentPlaceHolderID="TeacherCodeBox" runat="server">
    <h3>More examples:</h3>
<asp:CodeBox ID="Teacher_Database_Code" runat="server"  code="DatabaseCode" owner="teacher"></asp:CodeBox>
    <p>This statement returns the average time, rounded off to two decimal places, 
    of vehicle thefts for each month between 2014 and 2017. 
    The results are from the latest time of day to the earliest.</p>
</asp:Content>

<asp:Content ContentPlaceHolderID="Links" runat="server">
  <h3 class="text-center">Do you want to learn more about SQL Aggreagte functions? </h3>
<div class="text-center">
    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#sqllinks">CLICK HERE</button>
    <div id="sqllinks" class="collapse text-center">
     <ul class="list-group">
        <li class="list-group-item list-group-item-action""><a href="https://www.w3schools.com/sql/sql_count_avg_sum.asp">W3schools.com</a></li>
        <li class="list-group-item list-group-item-action""><a href="http://www.zentut.com/sql-tutorial/sql-aggregate-functions/">ZENTUT</a></li>
     </ul>
   </div>
</div>
</asp:Content>
