﻿<%@ Page Title="Web Application Development" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebAppDevelopment.aspx.cs" Inherits="Assignment2a.WebAppDevelopment" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
<h2>ASP.NET-Validators</h2>
    <h3>Overview</h3>
    <p>ASP.NETis a web application framework developed and marketed by Microsoft to allow programmers 
       to build dynamic web sites. It allows you to use a full featured programming language such as C# to build web applications.
       ASP.NET is used to produce interactive, data-driven web applications over the internet. It consists of a large number of controls such as text boxes, 
       buttons, and labels for assembling, configuring, and manipulating code to create HTML pages.</p>
       <p><strong>ASP.NET validation controls</strong> validate the user input data to ensure that useless, unauthenticated, or contradictory data don't get stored.</p>
       <p>ASP.NET provides the following validation controls:<br />
<span class="definition">RequiredFieldValidator</span> ensures that the required field is not empty. It is generally tied to a text box to force input into the text box.<br />
<span class="definition">RangeValidator</span> verifies that the input value falls within a predetermined range.
       It has three specific properties: <br />
       <i>Type</i> It defines the type of the data. The available values are: Currency, Date, Double, Integer, and String.
       <i>MinimumValue</i> It specifies the minimum value of the range.
       <i>MaximumValue</i> It specifies the maximum value of the range.<br />
<span class="definition">CompareValidator</span> compares a value in one control with a fixed value or a value in another control.<br />
<span class="definition">RegularExpressionValidator</span> allows validating the input text by matching against a pattern of a regular expression. 
       The regular expression is set in the ValidationExpression property.<br />
<span class="definition">CustomValidator</span> allows writing application specific custom validation routines for both the client side and the server side validation.
       The client side validation is accomplished through the ClientValidationFunction property. The client side validation routine should be written in a scripting language, 
       such as JavaScript, which the browser can understand. The server side validation routine must be called from the control's ServerValidate event handler. The server side 
       validation routine should be written in any .Net language, like C#.<br />
<span class="definition">ValidationSummary</span> does not perform any validation but shows a summary of all errors in the page. The summary displays the values of the ErrorMessage property 
       of all validation controls that failed validation.
    </p>
</asp:Content>
<asp:Content ID="WebAppExOne" ContentPlaceHolderID="MyCodeBox" runat="server">
    <h3>My examples:</h3>
    <asp:CodeBox ID="My_WebApp_Code" runat="server" code="WebAppCode" owner="me"></asp:CodeBox>
    <p> RequiredFieldValidator ensures that the required field is not empty
    In this example a user should enter his first name  
    </p> 
</asp:Content>
<asp:Content ID="WebAppExTwo" ContentPlaceHolderID="TeacherCodeBox" runat="server">
    <h3>More examples:</h3>
<asp:CodeBox ID="Teacher_Database_Code" runat="server"  code="WebAppCode" owner="teacher"></asp:CodeBox>
    <p>RegularExpressionValidator allows validating the input text by matching against a pattern of a regular expression.
 The regular expression is set in the ValidationExpression property.</p>
</asp:Content>

<asp:Content ContentPlaceHolderID="Links" runat="server">
  <h3 class="text-center">Do you want to learn more about Validators? </h3>
   <div class="text-center">
    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#sqllinks">CLICK HERE</button>
    <div id="sqllinks" class="collapse text-center">
     <ul class="list-group">
        <li class="list-group-item list-group-item-action""><a href="https://www.tutorialspoint.com/asp.net/asp.net_validators.htm">Tutorialspoint.com</a></li>
        <li class="list-group-item list-group-item-action""><a href="https://docs.microsoft.com/en-us/aspnet/overview">MICROSOFT
     </ul>
    </div>
</div>
</asp:Content>