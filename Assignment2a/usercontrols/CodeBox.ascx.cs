﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2a
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string Code
        {
            get { return (string)ViewState["Code"]; }
            set { ViewState["Code"] = value; }

        }

        public string Owner
        {
            get { return (string)ViewState["Owner"]; }
            set { ViewState["Owner"] = value; }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> codeToInsert;

            if (Code == "JsCode" && Owner == "me")
            {
                codeToInsert = new List<string>(new string[]{
                    "for (var i = 0; i < 10; i++){",

                    "~// check that the number is even",
                    "~if (i % 2 == 0){",
                        "~~continue;",
                    "~}",
                    "~// if we got here, then i is odd.",
                    "~console.log(i + \" is an odd number.\");",
                    "}",
                    "",
                " Output:",
               "~1 is an odd number.",
               "~3 is an odd number.",
               "~5 is an odd number.",
               "~7 is an odd number.",
               "~9 is an odd number."

                });
            }
            else if (Code == "JsCode" && Owner == "teacher")
            {
                codeToInsert = new List<string>(new string[] {
                    "var fruitsArray=[\"apple\", \"banana\", \"orange\", \"plum\", \"pineapple\"];",
                    "var i = 0;",
                    "while (i < fruitsArray.length) {",
                    "~console.log(\"Fruit at inddex# \" + i + \" is: \" + fruitsArray[i]);",
                    "~i++;",
                    "}",
                    "",
                    "Output: ",
                    "Fruit at inddex# 0 is: apple",
                    "Fruit at inddex# 1 is: banana",
                    "Fruit at inddex# 2 is: orange", 
                    "Fruit at inddex# 3 is: plum",
                    "Fruit at inddex# 4 is: pineapple"
            });
            }
            else if (Code == "DatabaseCode" && Owner == "me")
            {
                codeToInsert = new List<string>(new string[] {
                    "SELECT vendor_name, SUM(payment_total) AS total",
                    "FROM vendors v",
                    "JOIN invoices i",
                    "ON v.vendor_id=i.vendor_id",
                    "GROUP BY vendor_name",
                    "ORDER BY total DESC"
            });

            }
            else if (Code == "DatabaseCode" && Owner == "teacher")
            {
                codeToInsert = new List<string>(new string[] {
                    "SELECT ROUND(AVG(occurrencehour), 2) AS hour, occurrencemonth",
                    "FROM autotheft",
                    "WHERE occurrenceyear BETWEEN 2014 and 2017",
                    "GROUP BY occurrencemonth",
                    "ORDER BY hour DESC"
            });

            }
            else if (Code == "WebAppCode" && Owner == "me")
            {
                codeToInsert = new List<string>(new string[] {
                    "<div class=\"fname\">",
                    "~<asp:Label Text = \"First Name:\" runat=\"server\" /> <br />",
                    "~<asp:TextBox runat = \"server\" ID=\"clientfName\" placeholder=\"First Name\"> </asp:TextBox>",
                    "~<asp:RequiredFieldValidator runat = \"server\" ErrorMessage=\"Please enter your First Name\" ",
                    "~ControlToValidate=\"clientfName\" ID=\"validatorfName\"> </asp:RequiredFieldValidator>",
                    "</div>"
            });

            }
            else if (Code == "WebAppCode" && Owner == "teacher")
            {
                codeToInsert = new List<string>(new string[] {
                   "<div class=\"email\">",
                   "~<asp:Label Text = \"Email:\" runat=\"server\" /> <br />",
                   "~<asp:TextBox runat = \"server\" ID=\"clientEmail\" placeholder=\"Email\"></asp:TextBox>",
                   "~~<asp:RegularExpressionValidator ID = \"regexEmailValid\" runat=\"server\"", 
                   "~~ValidationExpression=\"|w+([-+.]|w+)*@|w+([-.]|w+)*|.|w+([-.]|w+)*\"", 
                   "~~ControlToValidate=\"clientEmail\" ErrorMessage=\"Invalid Email Format\"></asp:RegularExpressionValidator>",
                   "</div>"
            });

            }
            else
            {
                codeToInsert = new List<string>();
            }



            DataTable codeTable = new DataTable();
            DataColumn indexColumn = new DataColumn();
            DataColumn codeColumn = new DataColumn();



            indexColumn.ColumnName = "Line";
            indexColumn.DataType = System.Type.GetType("System.Int32");
            codeTable.Columns.Add(indexColumn);

            codeColumn.ColumnName = "Code";
            codeColumn.DataType = System.Type.GetType("System.String");
            codeTable.Columns.Add(codeColumn);

            DataRow coderow;

            int i = 0;
            foreach (string codeLine in codeToInsert)
            {
                coderow = codeTable.NewRow();
                coderow[indexColumn.ColumnName] = i;
                string formattedCode = System.Net.WebUtility.HtmlEncode(codeLine);
                formattedCode = formattedCode.Replace("~", "&nbsp;&nbsp;&nbsp");
                coderow[codeColumn.ColumnName] = formattedCode;


                i++;
                codeTable.Rows.Add(coderow);
            }

            DataView codeView = new DataView(codeTable);
            CodeBoxOne.DataSource = codeView;
            CodeBoxOne.DataBind();




        }
    }
}