﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assignment2a._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Study guide</h1>
        <p class="lead">You can find some useful information here.</p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Database Design</h2>
            <p>
                Here is some useful information about Aggregate Functions.
            </p>
            <p>
                <a class="btn btn-default" href="DatabaseDesign">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Web Programming</h2>
            <p>
                Here is some information about Loops in Javascript.
            </p>
            <p>
                <a class="btn btn-default" href="WebProgramming">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Web Application Development</h2>
            <p>
                Some useful information about Validators in ASP.NET
            </p>
            <p>
                <a class="btn btn-default" href="WebAppDevelopment">Learn more &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
