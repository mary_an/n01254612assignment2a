﻿<%@ Page Title="Web Programming" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebProgramming.aspx.cs" Inherits="Assignment2a.WebProgramming" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2>For Loop and While Loop in Java Script</h2>
    <h3>Overview</h3>
    <p>Loops are handy, if you want to run the same code over and over again, each time with a different value.
Loops can execute a block of code a number of times. Loops are often used with arrays or objects.</p>
     <p><strong>The For loop</strong> needs three things: initialization, condition, incrementer.
        <span class="definition">Initialization</span>  is a start value.
        <span class="definition">Condition</span> is a running condition (like If statement).
        <span class="definition">Incrementer</span> is a statement which increments the start value.</p>
        <pre>The for loop has the following syntax:
                for (initialization; condition; increment) {
                    code block to be executed 
            }      
       </pre>
    <p><strong>The While loop</strong> is very similar to the For loop, except it is often used when you do not 
        know how many times a loop should run, just that it should run as long as a sertain condition or state 
        is true. Another words the while loop is like the for loop with just one statement that is condition. The while loop 
        loops through a block of code as long as a specified condition is true.
    </p>
    <pre>The while loop has the following syntax:
            while (condition) {
                code block to be executed
            }
    </pre>
</asp:Content>
<asp:Content ID="WebProgrammingExOne" ContentPlaceHolderID="MyCodeBox" runat="server">
    <h3>My examples:</h3>
    <asp:CodeBox ID="My_JS_Code" runat="server" code="JsCode" owner="me"></asp:CodeBox>
</asp:Content>

<asp:Content ID="WebProgrammingExTwo" ContentPlaceHolderID="TeacherCodeBox" runat="server">
    <h3>More examples:</h3>
<asp:CodeBox ID="Teacher_JS_Code" runat="server"  code="JsCode" owner="teacher"></asp:CodeBox>
   </asp:Content>

<asp:Content ContentPlaceHolderID="Links" runat="server">
  <h3 class="text-center">Do you want to learn more about loops in Javascript? </h3>
   <div class="text-center">
    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#jslinks">CLICK HERE</button>
     <div id="jslinks" class="collapse text-center">
      <ul class="list-group">
        <li class="list-group-item list-group-item-action""><a href="https://www.w3schools.com/js/js_loop_while.asp">W3schools.com</a></li>
        <li class="list-group-item list-group-item-action""><a href="https://www.learn-js.org/en/Loops">Learn JS</a></li>
        <li class="list-group-item list-group-item-action""><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration">MDN web docs</a></li>
      </ul>
    </div>
   </div>
</asp:Content>